package cs.project;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClientMessenger implements Runnable
{
	ArrayList<Client> m_clients;
	private final static Lock m_clntListLock = new ReentrantLock();
	
	LinkedList<Message> m_messages;
	private final static Lock m_msgListLock = new ReentrantLock();	
	private final Condition hasContent = m_msgListLock.newCondition();
	
	
	
	public ClientMessenger()
	{
		m_clients = new ArrayList<Client>();
		m_messages = new LinkedList<Message>();
	}
	
	@Override
	public void run()
	{
		try
		{
			m_msgListLock.lock();
			
			while(!Thread.interrupted())
			{
				//Is there a message? Unlock and suspend until we are signaled that there is content
				while (m_messages.size() <= 0)
					hasContent.await();
				
				//Send every message in the list to every client
				m_clntListLock.lock();
				for (Message currMessage : m_messages)
				{
					for (Client currClient : m_clients)
					{
						currClient.send("MSG");
						currClient.send(currMessage.user);
						currClient.send("BEGIN");
						currClient.send(currMessage.msg);
						currClient.send("ENDMSG");
					}			
				}
				m_clntListLock.unlock();
				
				m_messages.clear(); //We have sent the messages, clear the list
			}
			
			m_msgListLock.unlock();
		}
		catch(Exception ex)
		{
			System.err.println("Unexpected exception occurred.");
			ex.printStackTrace();
		}
		finally
		{			
			m_clntListLock.unlock();
			m_msgListLock.unlock();
		}
	}
	
	public boolean hasClient (Client target)
	{
		boolean result = false;
		m_clntListLock.lock(); 
		result = m_clients.contains(target);
		m_clntListLock.unlock();
		return result;
	}
	
	public void remove (Client target)
	{
		m_clntListLock.lock(); //Remove client from client messaging list
		m_clients.remove(target);
		pushClientList();
		m_clntListLock.unlock();
	}
	
	public void add (Client user)
	{
		m_clntListLock.lock();  //Add client from client messaging list
		m_clients.add(user);
		pushClientList();
		m_clntListLock.unlock();
	}
	
	public void addMessage (Client user, String msg)
	{
		m_msgListLock.lock();
		System.out.println("[" + new Date().toString() + "] Msg (" + user.getUsername() + "): " + msg);
		m_messages.add(new Message(msg, user.getUsername()));
		hasContent.signalAll();  //Signal (likely the client messenger thread) that there is now a message
		m_msgListLock.unlock();
	}
	
	private void pushClientList()
	{
		m_clntListLock.lock();
		
		//Send list of clients to each client
		//Valid client list has the following format:
		// LIST\n
		// someusernamehere\n
		// anotheruser\n
		// <...n number of usernames, all newline terminated>
		// lastuser\n
		// ENDLIST\n
		
		for (Client currClient : m_clients)
		{
			currClient.send("LIST");
			
			for (Client temp : m_clients)
			{	
				currClient.send(temp.getUsername());
			}
			
			currClient.send("ENDLIST");			
		}
		m_clntListLock.unlock();
	}
	
	public void sendClose()
	{
		m_clntListLock.lock();
		
		//Send close signal to clients
		//Close connection signal is: CLOSE\n
		
		for (Client currClient : m_clients)
		{
			currClient.send("CLOSE");
		}
		m_clntListLock.unlock();
	}
	
	class Message
	{
		public String msg;
		public String user;
		
		public Message (String message, String whoSaidIt)
		{
			msg = message;
			user = whoSaidIt;
		}
	}
}
