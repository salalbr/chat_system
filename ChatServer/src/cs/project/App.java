package cs.project;

import cs.project.Server;
import java.io.IOException;
import java.util.Scanner;


public class App {
	public static void main(String args[])
	{
		try
		{    
			if(args.length >= 1 && args.length <= 2)
			{
				Thread server;
				if (args.length == 1)
				{
					server = new Thread(new Server(Integer.parseInt(args[0]), true));
				}
				else
				{
					boolean bindLocal = args[1] == "-e";						
					server = new Thread(new Server(Integer.parseInt(args[0]), bindLocal));
				}
				
				server.start();
				System.out.println("Press enter to exit the server");
				Scanner resp = new Scanner(System.in);
				resp.nextLine();
				server.interrupt();
				server.join();
			}
			else
			{
				System.out.println("Syntax for this command:  ChatServer.jar [port] [-e]");
				System.out.println("           port : Any valid port number");
				System.out.println("           -e   : Optional, signals the server to bind to this machine's actual IP address, not loopback.");
				System.out.println("                  If this option is not specified, the server will bind to default localhost/127.0.0.1");
			}
				
			System.exit(0);			
		}
		catch (IOException ex)
		{
			System.out.println("Binding of port " + args[0] + " failed.");
			ex.printStackTrace(); 
		}
		catch (NumberFormatException ex)
		{
			System.out.println("Specified port must be valid port number.");
		}
		catch (InterruptedException ex)
		{
			System.err.println("Main thread interrupted while attempting to end server thread.");
			System.err.println("Warning: Here there be zombies.");
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			System.err.println("Unexpected exception occurred.");
			ex.printStackTrace();	
		}
	}
}