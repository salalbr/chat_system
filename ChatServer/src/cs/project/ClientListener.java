package cs.project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.locks.Lock;

public class ClientListener implements Runnable 
{	
	private LinkedList<Thread> m_runningFileReaders; //Reference to all readers running on server
	private Lock m_listLock;						//Lock to access it
	private Client  m_client;
	private ClientMessenger m_messenger;
	private boolean m_abnormalClose;
	
	//*************************************************
	//	ClientListener{}
	//
	//  Used to receive messages from a Client using the Client object's internal
	//  SocketChannel-created Scanner and add it to the ClientMessenger 
	//  to be sent to all clients.
	//  Should only be started from server thread maintaining list of child threads and a
	//  queue of string messages to be sent out.
	//  On exit, removes self from list of running threads.
	//  Is interruptible, and dies on either an interrupt or normal completion.
	//*************************************************
	public ClientListener (Client newClient, LinkedList<Thread> runningClientHandlers, Lock listLock, ClientMessenger messenger)
	{
		m_runningFileReaders = runningClientHandlers;
		m_listLock = listLock;
		m_client = newClient;
		m_messenger = messenger;
		m_abnormalClose = false;
	}
	
	@Override
	public void run()
	{
		try
		{	
			String data = "";
			String username = "";
			String message = "";
			while (!Thread.interrupted())
			{
				//Valid messages have the following format:
				// MSG\n
				// someusernamehere\n
				// BEGIN\n
				// somemessagehere\n
				// ENDMSG\n
				
				BufferedReader clientIn = new BufferedReader(new InputStreamReader(m_client.getInputStream()));
				
				data = clientIn.readLine();
				if (data.equals("MSG"))
				{
					username = clientIn.readLine(); //Retrieve username from socket assigned to client
					
					if (!username.equals(m_client.getUsername())) //If username does not match name in local client object, discard incoming message as fraud.
						continue;
				
					data = clientIn.readLine();
					if (data.equals("BEGIN"))
					{
						message = clientIn.readLine();
						
						data = clientIn.readLine();
						if (data.equals("ENDMSG"))		//If we made it this far we now have an authenticated message
						{
							m_messenger.addMessage(m_client, message);  //Add the message to the messenger to send to everyone							
						}
					}
				}
				if (data.equals("CLOSE"))
					break;
			}			
		}
		catch(NullPointerException ex)
		{
			System.err.println("Exception Don't know why yet...");
			ex.printStackTrace();
		}
		catch(IOException ex)
		{
			//No line was found during a call to readLine(); the socket must have closed so tell log that it was abnormal
			m_abnormalClose = true;
		}
		finally
		{
			try
			{
				m_listLock.lock();
				
				//Remove client from list
				m_messenger.remove(m_client);
				
				//Close client's connection
				m_client.closeConn();
				
				if (m_abnormalClose)
					System.err.println("[" + new Date().toString() + "]" + " Client: " + m_client.getUsername() + " disconnected abnormally. (CLOSE not received from client before disconnect)");
				else
					System.out.println("[" + new Date().toString() + "]" + " Client: " + m_client.getUsername() + " disconnected.");
				
				//Remove handler from running threads/FileReaders list
				m_runningFileReaders.remove(m_runningFileReaders.indexOf(Thread.currentThread()));				
			}
			catch(IOException ex)
			{
				System.err.println("Exception during client SocketChannel close().");
				System.err.println("Failed to remove dying thread from list of threads.");
				ex.printStackTrace();
			}
			catch (IndexOutOfBoundsException ex)
			{
				System.err.println("Failed to remove dying thread from list of threads.");
				ex.printStackTrace();	
			}
			finally
			{
				m_listLock.unlock();
			}
		}
	}
}
