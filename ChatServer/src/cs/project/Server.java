package cs.project;

import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.Channels;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.net.InetAddress;
import java.net.InetSocketAddress;


//*************************************************
//	Server{}
//
//  Used as a listener for client connections and and dispatches
//  a ClientHandler thread to listen for messages.  When interrupted it 
//  will kill all child threads.  
//*************************************************
public class Server implements Runnable 
{
	private LinkedList<Thread> m_threads;
	private final static Lock m_listLock = new ReentrantLock();
	private ServerSocketChannel m_incomingConns;
	private SocketChannel m_currConn;
	
	private ClientMessenger m_messenger;
	private Thread m_messengerThread;
	
	
	public Server(int port, boolean bindExternAdd) throws IOException
	{
		m_threads = new LinkedList<Thread>();
		m_currConn = null;
		m_incomingConns = ServerSocketChannel.open();
		
		if (bindExternAdd)  //Bind to the actual external host address for this machine
		{
			InetAddress i = InetAddress.getLocalHost();
			m_incomingConns.socket().bind(new InetSocketAddress(i.getHostAddress(), port));
		}
		else      //Bind to the localhost/127.0.01 address
		{
			m_incomingConns.socket().bind(new InetSocketAddress("localhost", port));
		}
		
		m_messenger = new ClientMessenger();
		m_messengerThread = new Thread(m_messenger);		
	}
	
	@Override
	public void run()
	{
		Thread tempThread = new Thread();
		try
		{
			m_messengerThread.start(); //Start the messenger thread
			
			while(!Thread.interrupted())
			{
				m_currConn = m_incomingConns.accept();

				if (m_currConn != null)  //We have a new client connection, deal with it
				{
					PrintStream out = new PrintStream(Channels.newOutputStream(m_currConn));
					Scanner in = new Scanner(Channels.newInputStream(m_currConn));
					Client currClient = new Client(m_currConn);
					
					if (helloValid(in, out, currClient))
					{					
						//Add client to list if one by the same name isn't already connected
						if (!m_messenger.hasClient(currClient))
						{
							m_messenger.add(currClient);
							
							System.out.println("[" + new Date().toString() + "]" + " Client: " + currClient.getUsername() + " connected.");
							
							//Give new thread (1) Client (2) Thread list, so it can remove itself before it dies 
							//				  (3) Lock for thread list   (4) ClientMessenger to relay messages to
		
							tempThread = new Thread(new ClientListener(currClient, m_threads, m_listLock, m_messenger));
							
							m_listLock.lock();
							m_threads.add(tempThread);
							m_listLock.unlock();		
							
							tempThread.start(); //Start new client thread so it will listen for messages from client
						}
						else
							m_currConn.close();
					}
					else
						m_currConn.close();
				}
			}			
		}
		catch (ClosedByInterruptException ex)
		{
			Thread.interrupted(); //Clear the interrupt status so we can join on children with throwing an exception
		}
		catch (IOException ex)
		{
			System.err.println("IO error on connection.");
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			System.err.println("Unexpected exception occurred.");
			ex.printStackTrace();
		}
		finally
		{
			System.out.println("Server stopping...");
			System.out.println("Closing client connections...");
			try
			{
				if (cleanupChildren())
					System.out.println("Client connections closed successfully.");				
				m_incomingConns.close();  //Close the ServerSocket
			}
			catch(IOException ex)
			{
				System.err.println("Error closing ServerSocket.");
				ex.printStackTrace();			
			}			
			catch (Exception ex)
			{
				System.err.println("Unexpected exception occurred.");
				ex.printStackTrace();
			}
			finally
			{
				System.out.println("Server stopped.");
			}
		}
	}
	///////////////////////////////////////////////////
	//	helloValid()
	//
	//	Called when server has just accepted a connection
	//  to determine if the client is using our protocol and
	//  returns the relevant data to the server so it can dispatch
	//  a FileReader thread to receive the file;  It "answers the phone"
	///////////////////////////////////////////////////
	private boolean helloValid(Scanner in, PrintStream out, Client currClient)
	{
		try
		{
			String response = "";
			
			out.println("HELLO");
			
			response = in.nextLine();			
			if (response.equals("HITHERE"))					
			{
				out.println("OK");
				
				currClient.setUsername(in.nextLine());
				return true;
			} 		
		}
		catch (Exception ex)
		{
			return false;   //Any exception will translate to an invalid handshake
		}
		return false;
	}
	///////////////////////////////////////////////////
	//	cleanupChildren()
	//
	// Go through all threads, interrupt them and wait for them to die
	// This method is called after telling all clients to close to make
	// sure we have cleaned up all threads.
	// (They should remove themselves from the list of threads on closing)	
	///////////////////////////////////////////////////
	private boolean cleanupChildren()
	{	
		try
		{
			m_messenger.sendClose(); //Send close to all clients, see if they will close gracefully
			Thread.sleep(200);
			
			m_listLock.lock();
			int count =  m_threads.size();
			Thread head = m_threads.peek();
			
			while (count > 0 && head != null)
			{	
				if (head.isAlive())
				{
					m_listLock.unlock();	//Release control of the list so when child thread dies it can remove itself
					head.interrupt();
					head.join();
					m_listLock.lock();
				}
				else 
				{
					m_threads.remove(); //If for some reason there is a dead thread still in the list, remove it.  
				}					
				
				count =  m_threads.size();
				head = m_threads.peek();					
			}
			m_listLock.unlock();			
			return true;
		}
		catch (InterruptedException ex)
		{
			System.err.println("Server thread interrupted while closing child threads.");
			System.err.println("Warning: Here there be zombies.");
			ex.printStackTrace();
			return false;
		}
		/*finally
		{
			m_listLock.unlock(); // Not a good idea in this case.  Would throw an IllegalMonitorStateException if 
								 // InterruptedException was just caught because the only place that we might throw 
								 // an InterruptedException is from the join() and the only time we call join() is 
								 // if we have already unlocked the Lock.
		}
		*/	
	}
}