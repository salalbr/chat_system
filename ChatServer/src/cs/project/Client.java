package cs.project;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.SocketChannel;

public class Client 
{
	private SocketChannel m_conn;
	private String m_username;
	private boolean m_closedByServer;
		
	
	public Client (SocketChannel connection)
	{
		m_conn = connection;
		m_username = "";
		m_closedByServer = false;
	}
	
	public void setUsername(String name)
	{
		m_username = name;
	}	
		
	public InputStream getInputStream()
	{
		return Channels.newInputStream(m_conn);
	}
	
	public String getUsername()
	{
		return m_username;
	}
	
	public void closeConn() throws IOException
	{
		m_conn.close();
	}
	
	public void send(String message)
	{
		try
		{
			String data = message + System.getProperty("line.separator"); 
			
			m_conn.write(ByteBuffer.wrap(data.getBytes()));
		}
		catch(ClosedByInterruptException ex)
		{
			m_closedByServer = true;
		}
		catch(Exception ex)
		{
			if(!m_closedByServer)
			{
				System.err.println("Unkown Exception occurred while writing data to client.");
				System.err.println("Client: " + m_username);
				System.err.println("Data/Intended Message: " + message);
				ex.printStackTrace();
			}
		}
	}
	
	
	public String listen()
	{
		try
		{
			ByteBuffer bbBuff = ByteBuffer.allocate(2048);
			
			m_conn.configureBlocking(true);
			m_conn.read(bbBuff);
			
			return bbBuff.array().toString();
		}
		catch(Exception ex)
		{
			System.err.println("Exception occurred while reading data from client.");
			System.err.println("Client: " + m_username);
			ex.printStackTrace();
		}
		return null;
	}
	
	///////////////////////////////////////////////////
	//	equals()
	//
	//	According to this definition, a client is considered
	//  equal to another when their usernames are equivalent
	///////////////////////////////////////////////////
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) 
			return false;
	    if (obj == this) 
	    	return true;
	    if (this.getClass() != obj.getClass())
	    	return false;
	    
	    Client clntObj = (Client)obj;
	    
	    if(this.m_username.equals(clntObj.getUsername()))
	    	return true;
	    
	    return false;
	}
}
