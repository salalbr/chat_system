package cs3250;

import java.net.*;
import java.util.Scanner;
import java.io.*;

public class Client 
{
	//Connection related member variables
	public Socket client_connection;
	private String host;
	private int port;
	
	//User related stuff
	private String username;	
	
	//Reference to parent frame to pass to the ServerListener 
	//so it can add users and messages to the frame as they come in
	public ClientGUI frame;
	
	
	public Client(ClientGUI cg) 
	{
		frame = cg;		
		host = "localhost"; //defaults not really used
		port = 8090;
		username = "default";
	}
	
	public InputStream getInputStream() throws IOException
	{
		return client_connection.getInputStream();
	}
	
	public boolean notConnected()
	{
		return client_connection.isClosed();
	}
	
	public void sendMessage(String s) {
		try
		{
			PrintStream out = new PrintStream(client_connection.getOutputStream());
			out.println("MSG");
			out.println(username);
			out.println("BEGIN");
			out.println(s);
			out.println("ENDMSG");					
		}
		catch(Exception ex)
		{
			System.err.println("Unkown Exception occurred while trying to create local PrintStream to server.");
			System.err.println("Client: " + username);
			System.err.println("Data/Intended Message: " + s);
			ex.printStackTrace();
		}
	}
	public boolean connect(String tempUser, String tempHost, int tempPort) 
	{
		username = tempUser;
		host = tempHost;
		port = tempPort;
		boolean connection = false;
		
		try {
			 client_connection = new Socket(host,port);
		     Scanner s = new Scanner(client_connection.getInputStream());
		     PrintStream p = new PrintStream(client_connection.getOutputStream());
		     
		     String ack = s.nextLine();
		     if(!ack.equals("HELLO")) {
		    	 throw new Exception();
		     }
		     
		     p.println("HITHERE");
		     
		     ack = s.nextLine();
		     if(!ack.equals("OK")) {
		    	 throw new Exception();
		    	 
		     }
		     connection = true;
		     
		     p.println(username);
		     
		     Thread t1 = new Thread(new ServerListener(this, frame));
		     t1.start();
		     	    
		}
		catch (Exception e) {System.out.println("connection failed"); connection = false;}
		
		return connection;
	}
	
	public void closeConn() throws IOException
	{
		client_connection.close();
	}
	
}