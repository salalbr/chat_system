package cs3250;
import java.awt.*;
import cs3250.ClientGUI;

public class App {
	static ClientGUI frame;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                frame = new ClientGUI();
            }
        });
        
		
	}

}
