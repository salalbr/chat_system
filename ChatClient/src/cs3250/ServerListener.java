package cs3250;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ServerListener implements Runnable  {
	private BufferedReader server;
	private ClientGUI frame;
	
	public ServerListener(Client c, ClientGUI j) throws IOException {
		frame = j;
		server = new BufferedReader(new InputStreamReader(c.getInputStream()));
	}
	
	@Override
	public void run() {
		try
		{
			String data = "";
			while(true)
			{
				data = server.readLine();
				if(data.equals("LIST")) 
				{
					ArrayList<String> users = new ArrayList<String>();
					
					while(!data.equals("ENDLIST")) 
					{
						data = server.readLine();
						if(!data.equals( "ENDLIST"))
							users.add(data);
					}
					frame.updateUsers(users);
				}
				
				if(data.equals("MSG")) 
				{
					String msg = server.readLine() + " says:  "; //Add some syntactic sugar for display purposes
					
					data = server.readLine();
					if (data.equals("BEGIN"))
					{
						msg += server.readLine();
						
						data = server.readLine();
						if (data.equals("ENDMSG"))
							frame.updateMessages(msg);						
					}
				}
			}
		}
		catch(IOException ex)
		{
			//Socket was closed by client, just let the thread die after updating the GUI
			frame.onDisconnect();			
		}
		catch(NullPointerException ex)
		{
			//Connection was closed by server so we should probably just let the thread die after updating the GUI that the connection's toast
			frame.onDisconnect();
			frame.showServerDisconnected();
		}
	}

}
