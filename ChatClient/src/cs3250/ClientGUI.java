package cs3250;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ClientGUI extends JFrame{
	//Swing components
	private JPanel northPanel;
	private JPanel centerPanel;
	private JPanel southPanel;
	private JLabel connectionStatus;
	private JScrollPane scrollPaneLeft;
	private JScrollPane scrollPaneRight;
	private JTextArea usersList;
	private JTextArea messages;
	private JTextField msgField;
	private JButton send;
	private JMenuItem connectItem;
	private JMenuItem disconnectItem;
	
	//Other member variables
	private Client client;
	private final ArrayList<String> usersCache;
	private final ArrayList<String> messageCache;
	
	
	public ClientGUI()
	{
		this.setLocationByPlatform(true);
		this.setTitle("Super awesome chat program of SCIENCE!! -- CS3250 Java Programming Final Project-- (team names here) Justin Rohbock");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		messageCache = new ArrayList<String>();
		usersCache = new ArrayList<String>();
		
		client = new Client(this);
		Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        setSize(width - (width/5), height - (height/5));
        
        setMenuBar();
        setTopPanel();
        setCenterPanel();
        setSouthPanel();
        
        send.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
               client.sendMessage(msgField.getText());
            }
        });
        
        msgField.addKeyListener(new KeyListener()
        {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.getKeyCode() == (KeyEvent.VK_ENTER))
					client.sendMessage(msgField.getText());				
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// Intentionally empty
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// Intentionally empty				
			}
        });
	}
	
	public void onConnectionSuccess()
	{
		msgField.setEnabled(true);
		send.setEnabled(true);
		connectionStatus.setText("CONNECTED");
		disconnectItem.setEnabled(true);
		connectItem.setEnabled(false);
	}
	
	public void onDisconnect()
	{
		EventQueue.invokeLater(new Runnable()
        {
			public void run()
            {
				msgField.setEnabled(false);
				send.setEnabled(false);
				connectionStatus.setText("DISCONNECTED");
				disconnectItem.setEnabled(false);
				connectItem.setEnabled(true);
				usersList.setText(""); //Clear visible list of users
				messages.setText(""); //Clear visible list of message
				usersCache.clear();   //Clear the cache of users
				messageCache.clear(); //Clear the cache of messages
            }
        });
	}
	
	public void showServerDisconnected()
	{
		EventQueue.invokeLater(new Runnable()
        {
			public void run()
            {
				JOptionPane.showMessageDialog(null, "You have been disconnected by the server. What did you do!\n" +
						"Possibilities:\n" +
						"The server got tired of you and closed itself. (most likely)\n" +
						"The username you requested is already in use.\n" +
						"You messed up the hostname and/or port and need to check it. (use a CLI utility like ipconfig/ifconfig?)");
            }
        });
	}
	
	public void updateUsers(ArrayList<String> update)
	{	
		usersCache.clear();
		usersCache.addAll(update);		
		displayUsers();		
	}
	
	public void updateMessages(String newMessage)
	{
		messageCache.add(newMessage);
		displayMessages();		
	}
	
	public void displayUsers()
	{
		EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
            	usersList.setText("");
        		for(String user: usersCache)
        		{
        			usersList.append(user + '\n');
        		}
            }
        });		
	}
	
	public void displayMessages()
	{
		EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
            	messages.setText("");
        		for(String msg: messageCache)
        		{
        			messages.append(msg + '\n');
        		}
            }
        });		
	}

	public void showMessageBoxOnFail()
	{
		JOptionPane.showMessageDialog(ClientGUI.this, "Connection Failed");
	}

	public void setSouthPanel()
	{
		southPanel = new JPanel();
		msgField = new JTextField("Enter a message to send", 70);
		send = new JButton("Send");
		msgField.setEnabled(false);
		send.setEnabled(false);
		southPanel.add(msgField, BorderLayout.WEST);
		southPanel.add(send, BorderLayout.EAST);
		add(southPanel, BorderLayout.SOUTH);
	}
	public void setCenterPanel()
	{
		centerPanel = new JPanel();
		usersList = new JTextArea(30, 20);
		messages = new JTextArea(30, 60);
		usersList.setEditable(false);
		messages.setEditable(false);
		scrollPaneLeft = new JScrollPane(usersList);
		scrollPaneRight = new JScrollPane(messages);
		centerPanel.add(scrollPaneLeft, BorderLayout.WEST);
		centerPanel.add(scrollPaneRight, BorderLayout.EAST);
		add(centerPanel,BorderLayout.CENTER);
	}
	public void setTopPanel()
	{
		northPanel = new JPanel();
		connectionStatus = new JLabel("DISCONNECTED");
        northPanel.add(connectionStatus);
        northPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        add(northPanel,BorderLayout.NORTH);
	}
	public void setMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        connectItem = new JMenuItem("Connect");
        disconnectItem = new JMenuItem("Disconnect");
        disconnectItem.setEnabled(false);
        JMenuItem exitItem = new JMenuItem("Exit");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        connectItem.setMnemonic(KeyEvent.VK_C);
        disconnectItem.setMnemonic(KeyEvent.VK_D);
        exitItem.setMnemonic(KeyEvent.VK_E);        
        fileMenu.add(connectItem);
        fileMenu.add(disconnectItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
        
        connectItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	boolean cancelled = false;
            	boolean notValidNum = true;
            	String user = "default";
            	String host = "localhost";
            	int port = 0;
            	
            	user = JOptionPane.showInputDialog("Please enter user name");
            	if (user == null)
            		cancelled = true;
            	
            	if (cancelled == false)
            		host = JOptionPane.showInputDialog("Please enter hostname");  
            	if (host == null)
            		cancelled = true;
            	
            	if (cancelled == false)
        		{
	            	while(notValidNum)
	            	{
		            	try{
		            		String temp = JOptionPane.showInputDialog("Please enter port number");
	            			
	            			if (temp == null)
	            			{
		            			cancelled = true;
		            			break;
	            			}
	            			
	            			port = Integer.parseInt(temp);
		            		notValidNum = false;
		            	}		            	
		            	catch(NumberFormatException ex)
		            	{
		            		JOptionPane.showMessageDialog(null, "Invalid port number.");
		            	}
	            	}
            	}
            	
            	if (!cancelled)
            	{
	            	if(client.connect(user, host, port)) //If connection is successful
	        		{
	            		onConnectionSuccess(); //Update frame components to show that we are connected
	        		}
	            	else
	            		showMessageBoxOnFail();
            	}
            }
        });
               
        disconnectItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
				try {
					PrintStream p = new PrintStream (client.client_connection.getOutputStream());
					
					p.println("CLOSE"); //Ask the server to close the connection
					
					if (!client.notConnected())
						client.closeConn(); //Try to close it if it is still open
					
					onDisconnect();  //Update frame components to show that we are disconnected
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            }
        });
        
        exitItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
                System.exit(0);
            }
        });
        
        
    }
}
